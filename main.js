function timer(){
	let m1 = 0
	let m2 = 0 
	let s1 = 0
	let s2 = 0
	let ms1 = 0
	let ms2 = 0
	let deg = 0
	let minutesDeg = 0
	let milisecondesDeg = 0
	let block = document.createElement('div')
	block.id = 'main'
	document.body.appendChild(block)
	block.innerHTML = Number(m1)+''+ Number(m2) + ":" +  Number(s1)+ Number(s2) + ":" + Number(ms1)+Number(ms2)

	let clear = document.createElement('button')
	let start = document.createElement('button')
	let stop = document.createElement('button')

	stop.id ='stop'
	clear.id = 'clear'
	start.id = 'start'

	clear.innerHTML = 'Clear'
	start.innerHTML = 'Start'
	stop.innerHTML = 'Pause'

	document.body.appendChild(clear)
	document.body.appendChild(start)
	document.body.appendChild(stop)

	stop.style.display = 'none'

	
function interval(){
		block.innerHTML = Number(m1)+''+ Number(m2) + ":" +  Number(s1)+ Number(s2) + ":" + Number(ms1)+Number(ms2)
		ms2+=1
		milisecondesDeg+=3.6
		document.getElementById('milisecondes').style = 'transform: rotate(' + milisecondesDeg + 'deg)'
		if(ms2 % 10 == 0){
			ms1+=1
			ms2=0
			if(ms1 % 10 == 0){
				s2+=1
				ms1=0
				deg+=6
				document.getElementById('seconds').style = 'transform: rotate(' + deg + 'deg)'
				if(s2 % 10 == 0){
					s1+=1
					s2=0
					if(s1 % 6 == 0){
						m2+=1
						s1=0
						minutesDeg +=6
						document.getElementById('minutes').style = 'transform: rotate(' + minutesDeg + 'deg)'
						if(m2 % 10 == 0){
							m1+=1
							m2=0
						}
					}
				}
			}
		}
	}


	document.getElementById('start').onclick = function(){
		let int = setInterval(interval,10)
		start.style.display = 'none'
		stop.style.display = 'block'
		document.getElementById('stop').onclick = function(){
			clearInterval(int)
			stop.style.display = 'none'
			start.style.display = 'block'
		}
		document.getElementById('clear').onclick = function(){
			clearInterval(int)
			m1 = 0
			m2 = 0 
			s1 = 0
			s2 = 0
			ms1 = 0
			ms2 = 0
			deg = 0
			minutesDeg = 0
			block.innerHTML = Number(m1)+''+ Number(m2) + ":" +  Number(s1)+ Number(s2) + ":" + Number(ms1)+Number(ms2)
			stop.style.display = 'none'
			start.style.display = 'block'
			document.getElementById('seconds').style = 'transform: rotate(' + 0 + 'deg)'
			document.getElementById('minutes').style = 'transform: rotate(' + 0 + 'deg)'
			document.getElementById('milisecondes').style = 'transform: rotate(' + 0 + 'deg)'
		}

	}
}
timer()